package EMPLOYEES;//basic metric interface

import EMPLOYEES.Record;

public interface Metric {
	double getDistance(Record s, Record e);
}
