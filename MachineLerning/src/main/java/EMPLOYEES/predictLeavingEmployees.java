package EMPLOYEES;
import Jama.Matrix;
import smile.math.kernel.GaussianKernel;
import smile.classification.SVM;
import smile.classification.AdaBoost;
import smile.data.NumericAttribute;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class predictLeavingEmployees {
    public enum Department {
        sales,
        accounting,
        hr,
        technical,
        support,
        management,
        IT,
        product_mng,
        marketing,
        RandD;
    }
    public enum SalaryRate {
        low,
        medium,
        high;
    }
    //read the data from the CSV file to a 2D matrix
    public static double[][]  getMatrix() {
        String fileName= "employees.csv";
        File file= new File(fileName);
        List<List<String>> lines = new ArrayList<List<String>>();
        Scanner inputStream;

        try{
            inputStream = new Scanner(file);
            while(inputStream.hasNext()){
                String line= inputStream.next();
                String[] values = line.split(",");
                lines.add(Arrays.asList(values));
            }
            inputStream.close();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        double[][] matrix = new double[lines.size() - 1][lines.remove(0).size()];
        int j = 0;
        for(List<String> line: lines) {
            for (int i = 0; i < line.size() ; i++) { //iterate over the elements of the list
                if( i == line.size() - 2){
                    matrix [j][i] = (double) Department.valueOf(line.get(i)).ordinal();
                } else if(i == line.size() - 1){
                    matrix [j][i] = (double) SalaryRate.valueOf(line.get(i)).ordinal();
                } else {
                    matrix[j][i] = Double.parseDouble(line.get(i));
                }
            }
            j++;
        }
        //move the label column to the last column
        for (int i = 0; i < matrix.length ; i++) {
            double label = matrix[i][6];
            matrix[i][6] = matrix[i][matrix[i].length - 1];
            matrix[i][matrix[i].length - 1] = label;
        }
        return matrix;
    }

    private static double[][] getSamples(double[][] workers, int size) {
        int stay = 0, leave = 0;
        double[][] ans = new double[size * 2][workers[0].length];
        int j = 0;
        for (int i = 0; i < workers.length; i++) {
            if(j == size * 2){
                break;
            }
            if((workers[i][workers[0].length - 1] == 1 && leave < size) ||
                    (workers[i][workers[0].length - 1] == 0 && stay < size )){
                if(workers[i][workers[0].length - 1] == 1) leave ++;
                if(workers[i][workers[0].length - 1] == 0) stay ++;
                ans[j] = workers[i];
                j++;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        //the matrix contains the employees data, the last column is the label - 0/1 == stayed/left
        double[][] mat =  getMatrix();
        Matrix testData = new Matrix(new double[mat.length][mat[0].length]);
        for(int i = 0; i < mat.length; i++){
            for(int j = 0; j < mat[0].length; j++){
                testData.set(i,j,mat[i][j]);
            }
        }
        printToFile(mat, "employees_test.txt");

        double [][] sample = getSamples(mat, 1000);

        Matrix trainingData = new Matrix(new double[sample.length][sample[0].length]);
        for(int i = 0; i < sample.length; i++){
            for(int j = 0; j < sample[0].length; j++){
                trainingData.set(i,j,sample[i][j]);
            }
        }

        printToFile(sample, "employees_train.txt");

        knn k = new knn();
        System.out.println("Knn result with K = 1");
        k.knn("employees_train.txt","employees_test.txt",1,1);
        System.out.println();
        System.out.println("Knn result with K = 2");
        k.knn("employees_train.txt","employees_test.txt",2,1);
        System.out.println();
        System.out.println("Knn result with K = 3");
        k.knn("employees_train.txt","employees_test.txt",3,1);
        System.out.println();
        System.out.println("Knn result with K = 5");
        k.knn("employees_train.txt","employees_test.txt",5,1);
        System.out.println();
        System.out.println("Knn result with K = 10");
        k.knn("employees_train.txt","employees_test.txt",10,1);
        System.out.println();
        System.out.println("Knn result with K = 15");
        k.knn("employees_train.txt","employees_test.txt",15,1);
        System.out.println();
        System.out.println("Knn result with K = 20");
        k.knn("employees_train.txt","employees_test.txt",20,1);
        System.out.println();
        System.out.println("Knn result with K = 30");
        k.knn("employees_train.txt","employees_test.txt",30,1);
        System.out.println();
        System.out.println("Knn result with K = 50");
        k.knn("employees_train.txt","employees_test.txt",50,1);
        System.out.println();
        System.out.println("Knn result with K = 100");
        k.knn("employees_train.txt","employees_test.txt",100,1);
        System.out.println();


        double[][] x = new double[sample.length][sample[0].length - 1];
        for(int i = 0; i < sample.length; i++){
            for(int j = 0; j < sample[0].length - 1; j++){
                x[i][j] = sample[i][j];
            }
        }
        int[] y = new int [sample.length];
        for(int j = 0; j < sample.length; j++){
            if((int)sample[j][sample[0].length - 1] == 0)
            y[j] = 1;
            else y[j] = 2;
        }
        double[][] testx = new double[mat.length][mat[0].length - 1];
        for(int i = 0; i < mat.length; i++){
            for(int j = 0; j < mat[0].length - 1; j++){
                testx[i][j] = mat[i][j];
            }
        }
        int[] testy = new int [mat.length];
        for(int j = 0; j < mat.length; j++){
            if((int)mat[j][mat[0].length - 1] == 0)
                testy[j] = 1;
            else testy[j] = 2;
        }

        SVM<double[]> svm = new SVM<double[]>(new GaussianKernel(8.0), 5.0, 3, SVM.Multiclass.ONE_VS_ONE);
        svm.learn(x, y);
        svm.finish();

        int error = 0;
        for (int i = 0; i < testx.length; i++) {
            if (svm.predict(testx[i]) != testy[i]) {
                error++;
            }
        }
        System.out.println("SVM Algorithm");
        System.out.format("The accuracy is %.2f%%\n", 100.0 * (testx.length - error) / testx.length);
        System.out.println();

        for(int j = 0; j < sample.length; j++){
            if(y[j] == 1)
                y[j] = 0;
            else y[j] = 1;
        }
        AdaBoost forest = new AdaBoost(null, x, y, 100);
        for(int j = 0; j < testx.length; j++){
            if(testy[j] == 1)
                testy[j] = 0;
            else testy[j] = 1;
        }
        error = 0;
        for (int i = 0; i < testx.length; i++) {
            if (forest.predict(testx[i]) != testy[i]) {
                error++;
            }
        }
        System.out.println("ADA-BOOST Algorithm 100");
        System.out.format("The accuracy is %.2f%%\n", 100.0 * (testx.length - error) / testx.length);
        System.out.println();

        forest = new AdaBoost(null, x, y, 1000);
        error = 0;
        for (int i = 0; i < testx.length; i++) {
            if (forest.predict(testx[i]) != testy[i]) {
                error++;
            }
        }
        System.out.println("ADA-BOOST Algorithm 500");
        System.out.format("The accuracy is %.2f%%\n", 100.0 * (testx.length - error) / testx.length);
        System.out.println();
    }

    private static void printToFile(double[][] mat, String s) {
        try{
            PrintWriter writer = new PrintWriter(s, "UTF-8");
            int numOfCols = mat[0].length - 1;
            writer.println(mat.length + " " + numOfCols + " 1");
            for(int i = 0; i < mat.length; i++){
                for(int j = 0; j < mat[0].length; j++){
                    writer.print(mat[i][j] + " ");
                }
                writer.print("\n");
            }
            writer.close();
        }  catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}

