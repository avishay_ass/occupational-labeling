package EMPLOYEES;// compared with EMPLOYEES.Record, add another attribute - predictLabel
// which is used to store the predicted label for the current testRecord.

import EMPLOYEES.Record;

public class TestRecord extends Record {
	int predictedLabel;
	
	TestRecord(double[] attributes, int classLabel) {
		super(attributes, classLabel);
	}
}
